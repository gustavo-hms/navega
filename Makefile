all: reload

code := src/main.qml src/Nau.qml

build/navega.kwinscript: $(code)
	mkdir -p build/script/contents/code
	cp src/*.qml build/script/contents/code
	cp metadata.json build/script
	cd build/script; zip -r ../navega.kwinscript *

install: build/navega.kwinscript
	kpackagetool5 --type=KWin/Script -s navega \
		&& kpackagetool5 -u build/navega.kwinscript \
		|| kpackagetool5 --type=KWin/Script -i build/navega.kwinscript

uninstall:
	kpackagetool5 --type=KWin/Script -r navega

reload: install
	kwriteconfig5 --file kwinrc --group Plugins --key navegaEnabled true
	qdbus-qt5 org.kde.KWin /KWin reconfigure

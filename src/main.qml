import QtQuick 2.15

Item {
    id: caixa
    width: 1000
    height: 800
    x: 500
    y: 500
    
    property var clients: []
    
    Component.onCompleted: {
        workspace.clientAdded.connect(client => {
            if (client.normalWindow) {
                console.log(`Navega: ${client.caption}`)
                clients.push(client)
                clientsChanged()
            }
        });       
    }

    Row {
        anchors.fill: parent

        Repeater {
            model: caixa.clients

            Nau {
                client: caixa.clients[index]
            }
        }
    }
}

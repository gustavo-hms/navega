import QtQuick 2.15

Item {
    id: nau
    property var client
    
    Component.onCompleted: {
        client.geometry = {
            x: mapToGlobal(nau.x, nau.y).x,
            y: mapToGlobal(nau.x, nau.y).y,
            width: client.width,
            height: client.height
        }
    }
    
    onXChanged: {
        client.geometry = {
            x: mapToGlobal(nau.x, nau.y).x,
            y: client.geometry.y,
            width: client.geometry.width,
            height: client.geometry.height
        }
    }

    onYChanged: {
        client.geometry = {
            x: client.geometry.x,
            y: mapToGlobal(nau.x, nau.y).y,
            width: client.geometry.width,
            height: client.geometry.height
        }
    }

    onWidthChanged: {
        client.geometry = {
            x: client.geometry.x,
            y: client.geometry.y,
            width: nau.width,
            height: client.geometry.height
        }
    }

    onHeightChanged: {
        client.geometry = {
            x: client.geometry.x,
            y: client.geometry.y,
            width: client.geometry.width,
            height: nau.heigh
        }
    }
}
